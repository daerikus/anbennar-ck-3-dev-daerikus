﻿######################
# Anbennar Men-at-Arms Values #
######################

#########
# Costs #
#########

@high_maint_mult = 3

### Base Cost Values
@skirmisher_recruitment_cost = 45
@skirmisher_low_maint_cost = 0.15
@skirmisher_high_maint_cost = @[skirmisher_low_maint_cost * high_maint_mult]

@archers_recruitment_cost = 55
@archers_low_maint_cost = 0.2
@archers_high_maint_cost = @[archers_low_maint_cost * high_maint_mult]

@pikemen_recruitment_cost = 75
@pikemen_low_maint_cost = 0.3
@pikemen_high_maint_cost = @[pikemen_low_maint_cost * high_maint_mult]

@heavy_infantry_recruitment_cost = 90
@heavy_infantry_low_maint_cost = 0.4
@heavy_infantry_high_maint_cost = @[heavy_infantry_low_maint_cost * high_maint_mult]

@light_cavalry_recruitment_cost = 85
@light_cavalry_low_maint_cost = 0.35
@light_cavalry_high_maint_cost = @[light_cavalry_low_maint_cost * high_maint_mult]

@heavy_cavalry_recruitment_cost = 200
@heavy_cavalry_low_maint_cost = 0.7
@heavy_cavalry_high_maint_cost = @[heavy_cavalry_low_maint_cost * high_maint_mult]

@siege_weapons_recruitment_cost = 60
@siege_weapons_low_maint_cost = 0.1
@siege_weapons_high_maint_cost = @[siege_weapons_low_maint_cost * high_maint_mult]

@mages_recruitment_cost = 150
@mages_low_maint_cost = 0.5
@mages_high_maint_cost = @[mages_low_maint_cost * high_maint_mult]

@beast_recruitment_cost = 150
@beast_low_maint_cost = 0.7
@beast_high_maint_cost = @[beast_low_maint_cost * high_maint_mult]


### Anbennar Cultural MAA
dwarven_handgunner_recruitment_cost = @[archers_recruitment_cost * 6.36]
dwarven_handgunner_low_maint_cost = @[archers_low_maint_cost * 5]
dwarven_handgunner_high_maint_cost = @[archers_high_maint_cost * 5]

damerian_longbowmen_recruitment_cost = @[archers_recruitment_cost * 1.2]
damerian_longbowmen_low_maint_cost = @[archers_low_maint_cost * 1.2]
damerian_longbowmen_high_maint_cost = @[archers_high_maint_cost * 1.2]

oakfoot_rangers_recruitment_cost = @[archers_recruitment_cost * 0.7]
oakfoot_rangers_low_maint_cost = @[archers_low_maint_cost * 0.7]
oakfoot_rangers_high_maint_cost = @[archers_high_maint_cost * 0.7]

troll_hunters_recruitment_cost = @[archers_recruitment_cost * 1.1]
troll_hunters_low_maint_cost = @[archers_low_maint_cost * 1.1]
troll_hunters_high_maint_cost = @[archers_high_maint_cost * 1.1]

forest_stalkers_recruitment_cost = @[archers_recruitment_cost * 1.1]
forest_stalkers_low_maint_cost = @[archers_low_maint_cost * 1.1]
forest_stalkers_high_maint_cost = @[archers_high_maint_cost * 1.1]

arbalesters_recruitment_cost = @[archers_recruitment_cost * 1.6]
arbalesters_low_maint_cost = @[archers_low_maint_cost * 1.6]
arbalesters_high_maint_cost = @[archers_high_maint_cost * 1.6]

small_militia_recruitment_cost = @[skirmisher_recruitment_cost * 0.65]
small_militia_low_maint_cost = @[skirmisher_low_maint_cost * 0.65]
small_militia_high_maint_cost = @[skirmisher_high_maint_cost * 0.65]

woodsmen_recruitment_cost = @[skirmisher_recruitment_cost * 1.5]
woodsmen_low_maint_cost = @[skirmisher_low_maint_cost * 1.5]
woodsmen_high_maint_cost = @[skirmisher_high_maint_cost * 1.5]

agderbedderen_recruitment_cost = @[skirmisher_recruitment_cost * 1.4]
agderbedderen_low_maint_cost = @[skirmisher_low_maint_cost * 1.4]
agderbedderen_high_maint_cost = @[skirmisher_high_maint_cost * 1.4]

moormen_clan_raiders_recruitment_cost = @[skirmisher_recruitment_cost * 0.88]
moormen_clan_raiders_low_maint_cost = @[skirmisher_low_maint_cost * 0.88]
moormen_clan_raiders_high_maint_cost = @[skirmisher_high_maint_cost * 0.88]

alononan_recruitment_cost = @[skirmisher_recruitment_cost]
alononan_low_maint_cost = @[skirmisher_low_maint_cost]
alononan_high_maint_cost = @[skirmisher_high_maint_cost]

saerines_recruitment_cost = @[skirmisher_recruitment_cost * 1.5]
saerines_low_maint_cost = @[skirmisher_low_maint_cost * 1.5]
saerines_high_maint_cost = @[skirmisher_high_maint_cost * 1.5]

mistfighters_recruitment_cost = @[skirmisher_recruitment_cost * 1.4]
mistfighters_low_maint_cost = @[skirmisher_low_maint_cost * 1.4]
mistfighters_high_maint_cost = @[skirmisher_high_maint_cost * 1.4]

vernmen_veterans_recruitment_cost = @[skirmisher_recruitment_cost * 1.4]
vernmen_veterans_low_maint_cost = @[skirmisher_low_maint_cost * 1.4]
vernmen_veterans_high_maint_cost = @[skirmisher_high_maint_cost * 1.4]

dercinges_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.2]
dercinges_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.2]
dercinges_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.2]

wexonard_greatswords_recruitment_cost = @[heavy_infantry_recruitment_cost * 2]
wexonard_greatswords_low_maint_cost = @[heavy_infantry_low_maint_cost * 2]
wexonard_greatswords_high_maint_cost = @[heavy_infantry_high_maint_cost * 2]

patrician_guards_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.2]
patrician_guards_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.2]
patrician_guards_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.2]

ebonsteel_infantry_recruitment_cost = @[heavy_infantry_recruitment_cost * 4.4]
ebonsteel_infantry_low_maint_cost = @[heavy_infantry_low_maint_cost * 4.4]
ebonsteel_infantry_high_maint_cost = @[heavy_infantry_high_maint_cost * 4.4]

havoric_clansmen_recruitment_cost = @[heavy_infantry_recruitment_cost]
havoric_clansmen_low_maint_cost = @[heavy_infantry_low_maint_cost]
havoric_clansmen_high_maint_cost = @[heavy_infantry_high_maint_cost]

vrorengards_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.27]
vrorengards_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.27]
vrorengards_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.27]

balakuran_recruitment_cost = @[heavy_infantry_recruitment_cost * 0.88]
balakuran_low_maint_cost = @[heavy_infantry_low_maint_cost * 0.88]
balakuran_high_maint_cost = @[heavy_infantry_high_maint_cost * 0.88]

bladesworn_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.3]
bladesworn_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.3]
bladesworn_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.3]

spadiores_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.6]
spadiores_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.6]
spadiores_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.6]

taldoud_guard_recruitment_cost = @[heavy_infantry_recruitment_cost * 1.1]
taldoud_guard_low_maint_cost = @[heavy_infantry_low_maint_cost * 1.1]
taldoud_guard_high_maint_cost = @[heavy_infantry_high_maint_cost * 1.1]

arannese_bardiches_recruitment_cost = @[heavy_infantry_recruitment_cost]
arannese_bardiches_low_maint_cost = @[heavy_infantry_low_maint_cost]
arannese_bardiches_high_maint_cost = @[heavy_infantry_high_maint_cost]

mireknights_recruitment_cost = @[pikemen_recruitment_cost * 1.4]
mireknights_low_maint_cost = @[pikemen_low_maint_cost * 1.4]
mireknights_high_maint_cost = @[pikemen_high_maint_cost * 1.4]

isargaesos_recruitment_cost = @[pikemen_recruitment_cost * 1.5]
isargaesos_low_maint_cost = @[pikemen_low_maint_cost * 1.5]
isargaesos_high_maint_cost = @[pikemen_high_maint_cost * 1.5]

bulwari_phalanx_recruitment_cost = @[pikemen_recruitment_cost * 1.05]
bulwari_phalanx_low_maint_cost = @[pikemen_low_maint_cost * 1.05]
bulwari_phalanx_high_maint_cost = @[pikemen_high_maint_cost * 1.05]

gawedi_talonmen_recruitment_cost = @[pikemen_recruitment_cost * 1.25]
gawedi_talonmen_low_maint_cost = @[pikemen_low_maint_cost * 1.25]
gawedi_talonmen_high_maint_cost = @[pikemen_high_maint_cost * 1.25]

thorn_squares_recruitment_cost = @[pikemen_recruitment_cost * 1.6]
thorn_squares_low_maint_cost = @[pikemen_low_maint_cost * 1.6]
thorn_squares_high_maint_cost = @[pikemen_high_maint_cost * 1.6]

femaor_warriors_recruitment_cost = @[pikemen_recruitment_cost * 1.1]
femaor_warriors_low_maint_cost = @[pikemen_low_maint_cost * 1.1]
femaor_warriors_high_maint_cost = @[pikemen_high_maint_cost * 1.1]

burning_guard_recruitment_cost = @[pikemen_recruitment_cost * 1.3]
burning_guard_low_maint_cost = @[pikemen_low_maint_cost * 1.3]
burning_guard_high_maint_cost = @[pikemen_high_maint_cost * 1.3]

ribbonspears_recruitment_cost = @[pikemen_recruitment_cost * 1.6]
ribbonspears_low_maint_cost = @[pikemen_low_maint_cost * 1.6]
ribbonspears_high_maint_cost = @[pikemen_high_maint_cost * 1.6]

battlemages_recruitment_cost = @[mages_recruitment_cost * 1.6]
battlemages_low_maint_cost = @[mages_low_maint_cost * 1.6]
battlemages_high_maint_cost = @[mages_high_maint_cost * 1.6]

druids_recruitment_cost = @[mages_recruitment_cost * 1.3]
druids_low_maint_cost = @[mages_low_maint_cost * 1.3]
druids_high_maint_cost = @[mages_high_maint_cost * 1.3]

skalds_recruitment_cost = @[mages_recruitment_cost * 1.3]
skalds_low_maint_cost = @[mages_low_maint_cost * 1.3]
skalds_high_maint_cost = @[mages_high_maint_cost * 1.3]

storm_sorcerers_recruitment_cost = @[mages_recruitment_cost]
storm_sorcerers_low_maint_cost = @[mages_low_maint_cost]
storm_sorcerers_high_maint_cost = @[mages_high_maint_cost]

wren_hedge_witches_recruitment_cost = @[mages_recruitment_cost]
wren_hedge_witches_low_maint_cost = @[mages_low_maint_cost]
wren_hedge_witches_high_maint_cost = @[mages_high_maint_cost]

pony_riders_recruitment_cost = @[light_cavalry_recruitment_cost * 0.64]
pony_riders_low_maint_cost = @[light_cavalry_low_maint_cost * 0.64]
pony_riders_high_maint_cost = @[light_cavalry_high_maint_cost * 0.64]

wayknights_recruitment_cost = @[light_cavalry_recruitment_cost * 1.1]
wayknights_low_maint_cost = @[light_cavalry_low_maint_cost * 1.1]
wayknights_high_maint_cost = @[light_cavalry_high_maint_cost * 1.1]

rose_knights_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.25]
rose_knights_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.25]
rose_knights_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.25]

akalite_cataphracts_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.3]
akalite_cataphracts_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.3]
akalite_cataphracts_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.3]

black_knights_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.1]
black_knights_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.1]
black_knights_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.1]

knights_of_the_spear_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.1]
knights_of_the_spear_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.1]
knights_of_the_spear_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.1]

castellyrian_heavy_knights_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.1]
castellyrian_heavy_knights_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.1]
castellyrian_heavy_knights_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.1]

arbarani_heavy_riders_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.15]
arbarani_heavy_riders_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.15]
arbarani_heavy_riders_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.15]

war_mammoths_recruitment_cost = @[heavy_cavalry_recruitment_cost * 2.5]
war_mammoths_low_maint_cost = @[heavy_cavalry_low_maint_cost * 2.5]
war_mammoths_high_maint_cost = @[heavy_cavalry_high_maint_cost * 2.5]

griffon_riders_recruitment_cost = @[beast_recruitment_cost * 1.8]
griffon_riders_low_maint_cost = @[beast_low_maint_cost * 1.8]
griffon_riders_high_maint_cost = @[beast_high_maint_cost * 1.8]

wyvern_riders_recruitment_cost = @[beast_recruitment_cost * 1.3]
wyvern_riders_low_maint_cost = @[beast_low_maint_cost * 1.3]
wyvern_riders_high_maint_cost = @[beast_high_maint_cost * 1.3]

drake_riders_recruitment_cost = @[beast_recruitment_cost * 1.5]
drake_riders_low_maint_cost = @[beast_low_maint_cost * 1.5]
drake_riders_high_maint_cost = @[beast_high_maint_cost * 1.5]

salamander_riders_recruitment_cost = @[beast_recruitment_cost * 1.3]
salamander_riders_low_maint_cost = @[beast_low_maint_cost * 1.3]
salamander_riders_high_maint_cost = @[beast_high_maint_cost * 1.3]

dwarven_bombard_recruitment_cost = @[siege_weapons_recruitment_cost * 2]
dwarven_bombard_low_maint_cost = @[siege_weapons_low_maint_cost * 2]
dwarven_bombard_high_maint_cost = @[siege_weapons_high_maint_cost * 2]

### Regional MAA

alenic_outriders_recruitment_cost = @[light_cavalry_recruitment_cost * 0.58]
alenic_outriders_low_maint_cost = @[light_cavalry_low_maint_cost * 0.58]
alenic_outriders_high_maint_cost = @[light_cavalry_high_maint_cost * 0.58]

epuires_recruitment_cost = @[light_cavalry_recruitment_cost * 0.7]
epuires_low_maint_cost = @[light_cavalry_low_maint_cost * 0.7]
epuires_high_maint_cost = @[light_cavalry_high_maint_cost * 0.7]

businori_raiders_recruitment_cost = @[light_cavalry_recruitment_cost * 0.95]
businori_raiders_low_maint_cost = @[light_cavalry_low_maint_cost * 0.95]
businori_raiders_high_maint_cost = @[light_cavalry_high_maint_cost * 0.95]

knights_of_the_serpent_recruitment_cost = @[light_cavalry_recruitment_cost * 0.58]
knights_of_the_serpent_low_maint_cost = @[light_cavalry_low_maint_cost * 0.58]
knights_of_the_serpent_high_maint_cost = @[light_cavalry_high_maint_cost * 0.58]

flying_chariots_recruitment_cost = @[heavy_cavalry_recruitment_cost * 1.5]
flying_chariots_low_maint_cost = @[heavy_cavalry_low_maint_cost * 1.5]
flying_chariots_high_maint_cost = @[heavy_cavalry_high_maint_cost * 1.5]

###Accolade MaA



##############
# AI Weights #
##############

