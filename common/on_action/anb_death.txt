﻿
anb_on_death = {
	effect = {
		### Domination Ceases ###
		# Get all dominated persons (if any) then check if the are already in their story (and end it) or just give them the event
		every_hooked_character = {
			limit = { 
				prev = {
					has_hook_of_type = {
						type = dominate_hook
						target = this	
					}
				}
			}
			if = {	# In the story
				limit = {	
					any_owned_story = {
						story_type = anb_story_cycle_break_domination
					}
				}
				save_scope_as = dominate_story
				scope:dominate_story = {
					end_story = yes
				}
			}
			else = {
				trigger_event = anb_dominate_victim.5
			}
		}
		
		# Blademarches
		if = {
			limit = {
				has_character_modifier = bladesteward
			}
			title:k_blademarches.holder = {
				trigger_event = blademarches_succession.0005
			}
		}
	}
}