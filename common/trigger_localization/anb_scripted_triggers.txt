﻿
liege_is_illegitimate_race_for_title_trigger = {
	global = liege_is_illegitimate_race_for_title_trigger_tt
}

spell_already_being_cast_on = {
	global = spell_already_being_cast_on_tt
}

reveal_as_practitioner_decision_requirement = {
	global = reveal_as_practitioner_decision_requirement_tt
}

cannot_gift_calindal = {
	global = cannot_gift_calindal_tt
}

owns_calindal = {
	global = owns_calindal_tt
}

is_in_blademarches_kingdom = {
	global = is_in_blademarches_kingdom_tooltip
}

empty_castle_holding = {
	global = empty_castle_holding_tt
}

order_of_bladestewards_established = {
	global = order_of_bladestewards_established_tooltip
}
