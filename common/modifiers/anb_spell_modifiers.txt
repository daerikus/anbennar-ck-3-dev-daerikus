﻿#General spellcasting modifiers
general_spellcasting_tired = {
	icon = health_negative
	health = -0.25
}



#Enhance Ability
enhance_ability_diplomacy_success = {
	icon = diplomacy_positive
	diplomacy = 4
}

enhance_ability_martial_success = {
	icon = martial_positive
	martial = 4
}

enhance_ability_stewardship_success = {
	icon = stewardship_positive
	stewardship = 4
}

enhance_ability_intrigue_success = {
	icon = intrigue_positive
	intrigue = 4
}

enhance_ability_learning_success = {
	icon = learning_positive
	learning = 4
}

enhance_ability_prowess_success = {
	icon = prowess_positive
	prowess = 4
}

enhance_ability_diplomacy_fail = {
	icon = diplomacy_negative
	diplomacy = -2
}

enhance_ability_martial_fail = {
	icon = martial_negative
	martial = -2
}

enhance_ability_stewardship_fail = {
	icon = stewardship_negative
	stewardship = -2
}

enhance_ability_intrigue_fail = {
	icon = intrigue_negative
	intrigue = -2
}

enhance_ability_learning_fail = {
	icon = learning_negative
	learning = -2
}

enhance_ability_prowess_fail = {
	icon = prowess_negative
	prowess = -2
}
